package uppgift1;

import java.util.Locale;

@FunctionalInterface
interface Test {
    String apply(String x);
}

public class Main {

    public static void main(String[] args) {

        useMyInterface(x -> x, "hello word");



    }

    public static void useMyInterface(Test test, String x) {
        System.out.println(test.apply(x).toUpperCase(Locale.ROOT));

    }


}
