package Uppgift4;

import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Employee> employeeList = List.of(
                new Employee("Harris", 25, 10000000),
                new Employee("Patrik", 24, 34450),
                new Employee("Karl", 21, 21212),
                new Employee("Filip", 34, 1202002),
                new Employee("Albin", 22, 122920)
                );

        employeeList.stream()
                .sorted(Comparator.comparing(Employee::getSalary).reversed())
                .limit(3)
                .sorted(Comparator.comparing(Employee::getAge))
                .skip(1)
                .limit(1)
                .forEach(System.out::println);
    }
}
