package Uppgift2;

import java.util.function.Consumer;
import java.util.function.Function;

public class Main {

    public static void main(String[] args) {

        Function<Integer, Integer> addFive = x-> Integer.sum(x,5);
        Function<Double,Double> squirt = x-> {System.out.println(Math.sqrt(x));return x;};
        Consumer<Integer> printDoubleInt = x-> System.out.println(x * 2);
    }
}
